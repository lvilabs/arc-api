<?php

namespace Test\ArcApi;

use ArcApi\Client;
use ArcApi\Exception\InvalidArgumentException;
use GuzzleHttp\Client as BaseClient;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;

class ClientTest extends TestCase
{

    public function testCannotCreateClientWithEmptyOrgName()
    {
        $this->expectException(InvalidArgumentException::class);
        new Client();
    }

    public function testCreateClientDefinesHttpClient()
    {
        $orgName = 'test';
        $client = new Client(['org_name' => $orgName]);
        $this->assertInstanceOf(BaseClient::class, $client->getHttpClient());
        $this->assertEquals($orgName, $client->getHttpClient()->getConfig('org_name'));
        $url = "https://api.$orgName.arcpublishing.com";
        $this->assertEquals($url, $client->getHttpClient()->getConfig('base_uri'));
    }

    public function testWrongOrgName()
    {
        $mock = new MockHandler([
            new ConnectException('Could not resolve host: api.test.arcpublishing.com', new Request('GET', '/'))
        ]);
        $handlerStack = HandlerStack::create($mock);
        $this->expectException(ConnectException::class);
        $client = new Client(['org_name' => 'test', 'handler' => $handlerStack]);
        $client->getHttpClient()->get('/');
    }

    public function testNotAuthorized()
    {
        $mock = new MockHandler([
            new Response(401, [], '401 Authorization Required')
        ]);
        $handlerStack = HandlerStack::create($mock);
        $this->expectException(ClientException::class);
        $client = new Client(['org_name' => 'test', 'handler' => $handlerStack]);
        $client->getHttpClient()->get('/');
    }

    public function testAuthorized()
    {
        $mock = new MockHandler([
            new Response(200)
        ]);
        $handlerStack = HandlerStack::create($mock);
        $client = new Client([
            'org_name' => 'test',
            'auth' => ['user', 'password'],
            'handler' => $handlerStack
        ]);
        $response = $client->getHttpClient()->get('/');
        $this->assertEquals(200, $response->getStatusCode());
    }

}
