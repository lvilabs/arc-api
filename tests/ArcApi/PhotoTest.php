<?php


namespace Test\ArcApi;

use ArcApi\Exception\FileNotFoundException;
use ArcApi\Photo;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;

class PhotoTest extends TestCase
{

    public function testIndex()
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/photo_index_ok.json");
        $mock = new MockHandler([
            new Response(200, [], $responseData)
        ]);
        $handlerStack = HandlerStack::create($mock);
        $client = new Photo([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack
        ]);
        $response = $client->index();
        $data = json_decode($response->getBody()->getContents());
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertCount(1, $data);
        $this->assertEquals("ABCDEFGHIJKLMNOP1234567890", $data[0]->_id);
    }

    public function testCreateByANSBadRequest()
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/photo_index_ok.json");
        $mock = new MockHandler([
            new Response(200, [], $responseData)
        ]);
        $handlerStack = HandlerStack::create($mock);
        $this->expectException(FileNotFoundException::class);
        $client = new Photo([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack
        ]);
        $client->createByANS("non-file");
    }

    public function testCreateByANSBadANS()
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/photo_create_bad_ans.json");
        $mock = new MockHandler([
            new Response(400, [], $responseData)
        ]);
        $handlerStack = HandlerStack::create($mock);
        $this->expectException(ClientException::class);
        $client = new Photo([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack
        ]);
        $client->createByANS(__DIR__ . "/../stubs/test_bad_image.json");
    }

    public function testCreateByFileBadRequest()
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/photo_index_ok.json");
        $mock = new MockHandler([
            new Response(200, [], $responseData)
        ]);
        $handlerStack = HandlerStack::create($mock);
        $this->expectException(FileNotFoundException::class);
        $client = new Photo([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack
        ]);
        $client->createByFile("non-file.jpg");
    }

    public function testCreateByFileBadImage()
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/photo_create_bad_image.json");
        $mock = new MockHandler([
            new Response(400, [], $responseData)
        ]);
        $handlerStack = HandlerStack::create($mock);
        $this->expectException(ClientException::class);
        $client = new Photo([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack
        ]);
        $client->createByFile(__DIR__ . "/../stubs/test.jpg");
    }

    public function testCreateByANSOk()
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/photo_create_ok.json");
        $mock = new MockHandler([
            new Response(200, [], $responseData)
        ]);
        $handlerStack = HandlerStack::create($mock);
        $client = new Photo([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack
        ]);

        $response = $client->createByANS(__DIR__ . "/../stubs/test_image.json");
        $data = json_decode($response->getBody()->getContents());
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals("ABCDEFGHIJKLMNOP1234567890", $data->_id);
        $this->assertEquals("image", $data->type);
        $this->assertEquals("test.jpg", $data->additional_properties->originalName);
    }

    public function testCreateByFileOk()
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/photo_create_ok.json");
        $mock = new MockHandler([
            new Response(200, [], $responseData)
        ]);
        $handlerStack = HandlerStack::create($mock);
        $client = new Photo([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack
        ]);

        $response = $client->createByFile(__DIR__ . "/../stubs/test.jpg");
        $data = json_decode($response->getBody()->getContents());
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals("ABCDEFGHIJKLMNOP1234567890", $data->_id);
        $this->assertEquals("image", $data->type);
        $this->assertEquals("test.jpg", $data->additional_properties->originalName);
    }

    public function testViewNotFound()
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/photo_view_not_found.json");
        $mock = new MockHandler([
            new Response(404, [], $responseData)
        ]);
        $handlerStack = HandlerStack::create($mock);
        $this->expectException(ClientException::class);
        $client = new Photo([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack
        ]);

        $client->view("ABCDEFGHIJKLMNOP1234567890");
    }

    public function testViewOk()
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/photo_view_ok.json");
        $mock = new MockHandler([
            new Response(200, [], $responseData)
        ]);
        $handlerStack = HandlerStack::create($mock);
        $client = new Photo([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack
        ]);

        $response = $client->view("ABCDEFGHIJKLMNOP1234567890");
        $data = json_decode($response->getBody()->getContents());
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals("ABCDEFGHIJKLMNOP1234567890", $data->_id);
        $this->assertEquals("image", $data->type);
        $this->assertEquals("test.jpg", $data->additional_properties->originalName);
    }

    public function testPublishNotFound()
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/photo_delete_not_found.json");
        $mock = new MockHandler([
            new Response(404, [], $responseData)
        ]);
        $handlerStack = HandlerStack::create($mock);
        $this->expectException(ClientException::class);
        $client = new Photo([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack
        ]);
        $client->publish("ABCDEFGHIJKLMNOP1234567890");
    }

    public function testPublishOk()
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/photo_publish_ok.json");
        $mock = new MockHandler([
            new Response(200, [], $responseData)
        ]);
        $handlerStack = HandlerStack::create($mock);
        $client = new Photo([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack
        ]);
        $response = $client->publish("ABCDEFGHIJKLMNOP1234567890");
        $data = json_decode($response->getBody()->getContents());
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals(true, $data->additional_properties->published);
    }

    public function testPublishAlreadyPublished()
    {
        $mock = new MockHandler([
            new Response(204, [], "")
        ]);
        $handlerStack = HandlerStack::create($mock);
        $client = new Photo([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack
        ]);
        $response = $client->publish("ABCDEFGHIJKLMNOP1234567890");
        $this->assertEquals(204, $response->getStatusCode());
        $this->assertEquals("", $response->getBody()->getContents());
    }

    public function testDeleteNotFound()
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/photo_delete_not_found.json");
        $mock = new MockHandler([
            new Response(404, [], $responseData)
        ]);
        $handlerStack = HandlerStack::create($mock);
        $this->expectException(ClientException::class);
        $client = new Photo([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack
        ]);

        $client->delete("ABCDEFGHIJKLMNOP1234567890");
    }

    public function testDeleteOk()
    {
        $mock = new MockHandler([new Response(204, [], "")]);
        $handlerStack = HandlerStack::create($mock);
        $client = new Photo([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack
        ]);

        $response = $client->delete("ABCDEFGHIJKLMNOP1234567890");
        $this->assertEquals(204, $response->getStatusCode());
        $this->assertEquals("", $response->getBody()->getContents());
    }

}