<?php


namespace Test\ArcApi;

use ArcApi\DraftRevision;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;

class DraftRevisionTest extends TestCase
{

    public function testIndexStoryNotFound()
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/draft_revision_story_not_found.json");
        $mock = new MockHandler([
            new Response(404, [], $responseData)
        ]);
        $handlerStack = HandlerStack::create($mock);
        $this->expectException(ClientException::class);
        $client = new DraftRevision([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack
        ]);
        $client->index("ABCDEFGHIJKLMNOP1234567890");
    }

    public function testIndexOk()
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/draft_revision_ok.json");
        $mock = new MockHandler([
            new Response(200, [], $responseData)
        ]);
        $handlerStack = HandlerStack::create($mock);
        $client = new DraftRevision([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack
        ]);
        $response = $client->index("ABCDEFGHIJKLMNOP1234567890");
        $data = json_decode($response->getBody()->getContents());
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertCount(1, $data->revisions);
        $this->assertEquals("DRAFT", $data->revisions[0]->type);
    }

    /**
     * @dataProvider updateBadRequestDataProvider
     */
    public function testUpdateBadRequest($data, $responseFile)
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/$responseFile.json");
        $mock = new MockHandler([
            new Response(400, [], $responseData)
        ]);
        $handlerStack = HandlerStack::create($mock);
        $this->expectException(ClientException::class);
        $client = new DraftRevision([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack
        ]);
        $client->update("ABCDEFGHIJKLMNOP1234567890", $data);
    }

    public function testUpdateNotFound()
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/draft_revision_update_not_found.json");
        $mock = new MockHandler([
            new Response(404, [], $responseData)
        ]);
        $handlerStack = HandlerStack::create($mock);
        $this->expectException(ClientException::class);
        $client = new DraftRevision([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack
        ]);

        $client->update("ABCDEFGHIJKLMNOP1234567890", ['ans' => ['canonical_website' => 'test', 'version' => '0.10.4', 'type' => 'story']]);
    }

    public function testUpdateOk()
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/draft_revision_update_ok.json");
        $mock = new MockHandler([
            new Response(200, [], $responseData)
        ]);
        $handlerStack = HandlerStack::create($mock);
        $client = new DraftRevision([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack
        ]);

        $response = $client->update("ABCDEFGHIJKLMNOP1234567890", ['ans' => ['canonical_website' => 'test', 'version' => '0.10.4', 'type' => 'story']]);
        $data = json_decode($response->getBody()->getContents());
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals("ABCDEFGHIJKLMNOP1234567890", $data->document_id);
        $this->assertEquals("test", $data->ans->canonical_website);
    }

    public function testPublishStoryNotFound()
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/draft_revision_publish_story_not_found.json");
        $mock = new MockHandler([
            new Response(404, [], $responseData)
        ]);
        $handlerStack = HandlerStack::create($mock);
        $this->expectException(ClientException::class);
        $client = new DraftRevision([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack
        ]);
        $client->publish("ABCDEFGHIJKLMNOP1234567890");
    }

    public function testPublishStoryOk()
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/draft_revision_publish_ok.json");
        $mock = new MockHandler([
            new Response(200, [], $responseData)
        ]);
        $handlerStack = HandlerStack::create($mock);
        $client = new DraftRevision([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack
        ]);
        $response = $client->publish("ABCDEFGHIJKLMNOP1234567890");
        $data = json_decode($response->getBody()->getContents());
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals("ABCDEFGHIJKLMNOP1234567890", $data->document_id);
        $this->assertEquals("PUBLISHED", $data->type);
    }

    public function updateBadRequestDataProvider()
    {
        return [
            [[], 'draft_revision_update_bad_request_unable_to_decode_revision'],
            [['document_id' => 'ABCDEFGHIJKLMNOP1234567890'], 'draft_revision_update_bad_request_required_ans'],
            [['ans' => []], 'draft_revision_update_bad_request_invalid_ans'],
            [['ans' => ['canonical_website' => 'la-voz']], 'draft_revision_update_bad_request_invalid_ans_version'],
            [['ans' => ['canonical_website' => 'la-voz', 'version' => '0.10.4']], 'draft_revision_update_bad_request_invalid_ans_content_type'],
        ];
    }

}