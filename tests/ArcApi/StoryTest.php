<?php

namespace Test\ArcApi;

use function count;
use ArcApi\Story;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;

class StoryTest extends TestCase
{

    public function testIndexOk()
    {
        $responseData = file_get_contents(__DIR__ . '/../stubs/index_ok.json');
        $mock = new MockHandler([
            new Response(200, [], $responseData)
        ]);
        $handlerStack = HandlerStack::create($mock);
        $client = new Story([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack
        ]);
        $response = $client->index();
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals(2, count(json_decode($response->getBody()->getContents(), true)));
    }

    /**
     * @dataProvider createBadRequestDataProvider
     */
    public function testCreateBadRequest($data, $responseFile)
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/$responseFile.json");
        $mock = new MockHandler([
            new Response(400, [], $responseData)
        ]);
        $handlerStack = HandlerStack::create($mock);
        $this->expectException(ClientException::class);
        $client = new Story([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack
        ]);
        $client->create($data);
    }

    public function testCreateOk()
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/create_ok.json");
        $mock = new MockHandler([
            new Response(201, [], $responseData)
        ]);
        $handlerStack = HandlerStack::create($mock);
        $client = new Story([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack
        ]);
        $response = $client->create(['version' => '0.10.4', 'type' => 'story']);
        $data = json_decode($response->getBody()->getContents());
        $this->assertEquals('ABCDEFGHIJKLMNOP1234567890', $data->_id);
        $this->assertEquals('0.10.4', $data->version);
        $this->assertEquals('story', $data->type);
    }

    public function createBadRequestDataProvider()
    {
        return [
            [[], 'create_bad_request_missing_version'],
            [['version' => '0.10.4'], 'create_bad_request_missing_type'],
        ];
    }

}
