<?php


namespace Test\ArcApi;

use ArcApi\DraftCirculation;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;

class DraftCirculationTest extends TestCase
{

    public function testIndexStoryNotFound()
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/draft_circulation_story_not_found.json");
        $mock = new MockHandler([
            new Response(404, [], $responseData)
        ]);
        $handlerStack = HandlerStack::create($mock);
        $this->expectException(ClientException::class);
        $client = new DraftCirculation([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack
        ]);
        $client->index("ABCDEFGHIJKLMNOP1234567890");
    }

    public function testIndexOk()
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/draft_circulation_ok.json");
        $mock = new MockHandler([
            new Response(200, [], $responseData)
        ]);
        $handlerStack = HandlerStack::create($mock);
        $client = new DraftCirculation([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack
        ]);
        $response = $client->index("ABCDEFGHIJKLMNOP1234567890");
        $data = json_decode($response->getBody()->getContents());
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertCount(1, $data->circulations);
        $this->assertEquals("ABCDEFGHIJKLMNOP1234567890", $data->circulations[0]->document_id);
    }

    public function testViewStoryNotFound()
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/draft_circulation_view_story_not_found.json");
        $mock = new MockHandler([
            new Response(404, [], $responseData)
        ]);
        $handlerStack = HandlerStack::create($mock);
        $this->expectException(ClientException::class);
        $client = new DraftCirculation([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack
        ]);
        $client->view("ABCDEFGHIJKLMNOP1234567890", "test");
    }

    public function testViewCirculationNotFound()
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/draft_circulation_view_circulation_not_found.json");
        $mock = new MockHandler([
            new Response(404, [], $responseData)
        ]);
        $handlerStack = HandlerStack::create($mock);
        $this->expectException(ClientException::class);
        $client = new DraftCirculation([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack
        ]);
        $client->view("ABCDEFGHIJKLMNOP1234567890", "test");
    }

    public function testViewOk()
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/draft_circulation_view_ok.json");
        $mock = new MockHandler([
            new Response(200, [], $responseData)
        ]);
        $handlerStack = HandlerStack::create($mock);
        $client = new DraftCirculation([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack
        ]);
        $response = $client->view("ABCDEFGHIJKLMNOP1234567890", "test");
        $data = json_decode($response->getBody()->getContents());
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals("ABCDEFGHIJKLMNOP1234567890", $data->document_id);
        $this->assertEquals("test", $data->website_id);
    }

    public function testCirculateBadRequest()
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/draft_circulation_circulate_bad_request.json");
        $mock = new MockHandler([
            new Response(400, [], $responseData)
        ]);
        $handlerStack = HandlerStack::create($mock);
        $this->expectException(ClientException::class);
        $client = new DraftCirculation([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack
        ]);
        $client->circulate("ABCDEFGHIJKLMNOP1234567890", "test", []);
    }

    public function testCirculateWebsiteNotFound()
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/draft_circulation_circulate_website_not_found.json");
        $mock = new MockHandler([
            new Response(404, [], $responseData)
        ]);
        $handlerStack = HandlerStack::create($mock);
        $this->expectException(ClientException::class);
        $client = new DraftCirculation([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack
        ]);
        $client->circulate("ABCDEFGHIJKLMNOP1234567890", "test", ["document_id" => "ABCDEFGHIJKLMNOP1234567890"]);
    }

    public function testCirculateStoryNotFound()
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/draft_circulation_circulate_story_not_found.json");
        $mock = new MockHandler([
            new Response(404, [], $responseData)
        ]);
        $handlerStack = HandlerStack::create($mock);
        $this->expectException(ClientException::class);
        $client = new DraftCirculation([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack,
        ]);
        $client->circulate("ABCDEFGHIJKLMNOP1234567890", "test", ["document_id" => "ABCDEFGHIJKLMNOP1234567890"]);
    }

    public function testCirculateOk()
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/draft_circulation_circulate_ok.json");
        $mock = new MockHandler([
            new Response(200, [], $responseData)
        ]);
        $handlerStack = HandlerStack::create($mock);
        $client = new DraftCirculation([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack,
        ]);
        $response = $client->circulate("ABCDEFGHIJKLMNOP1234567890", "test", ["document_id" => "ABCDEFGHIJKLMNOP1234567890"]);
        $data = json_decode($response->getBody()->getContents());
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals("test", $data->website_id);
        $this->assertEquals("ABCDEFGHIJKLMNOP1234567890", $data->document_id);
    }

}