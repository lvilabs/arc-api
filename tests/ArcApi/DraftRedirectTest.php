<?php


namespace Test\ArcApi;

use ArcApi\DraftRedirect;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;

class DraftRedirectTest extends TestCase
{

    public function testIndexStoryNotFound()
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/draft_redirect_story_not_found.json");
        $mock = new MockHandler([
            new Response(404, [], $responseData)
        ]);
        $handlerStack = HandlerStack::create($mock);
        $this->expectException(ClientException::class);
        $client = new DraftRedirect([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack,
        ]);
        $client->index("ABCDEFGHIJKLMNOP1234567890", "test");
    }

    public function testIndexWebsiteNotFound()
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/draft_redirect_website_not_found.json");
        $mock = new MockHandler([
            new Response(404, [], $responseData)
        ]);
        $handlerStack = HandlerStack::create($mock);
        $this->expectException(ClientException::class);
        $client = new DraftRedirect([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack,
        ]);
        $client->index("ABCDEFGHIJKLMNOP1234567890", "test");
    }

    public function testIndexOk()
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/draft_redirect_ok.json");
        $mock = new MockHandler([
            new Response(200, [], $responseData)
        ]);
        $handlerStack = HandlerStack::create($mock);
        $client = new DraftRedirect([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack
        ]);
        $response = $client->index("CUT7EFQTDBCNFDPVTEAY2QSQHQ", "la-voz");
        $data = json_decode($response->getBody()->getContents());
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertCount(1, $data->redirects);
    }

    public function testViewWebsiteNotFound()
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/draft_redirect_view_not_found.json");
        $mock = new MockHandler([
            new Response(404, [], $responseData)
        ]);
        $handlerStack = HandlerStack::create($mock);
        $this->expectException(ClientException::class);
        $client = new DraftRedirect([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack
        ]);
        $client->view("wrong-test", "/test/test-redirect/");
    }

    public function testViewUrlNotFound()
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/draft_redirect_view_not_found.json");
        $mock = new MockHandler([
            new Response(404, [], $responseData)
        ]);
        $handlerStack = HandlerStack::create($mock);
        $this->expectException(ClientException::class);
        $client = new DraftRedirect([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack
        ]);
        $client->view("test", "/test/wrong-redirect/");
    }

    public function testCreateWebsiteNotFound()
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/draft_redirect_create_website_not_found.json");
        $mock = new MockHandler([
            new Response(404, [], $responseData)
        ]);
        $handlerStack = HandlerStack::create($mock);
        $this->expectException(ClientException::class);
        $client = new DraftRedirect([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack
        ]);
        $client->create("test", "/test/test-redirect/", ["redirect_to" => "https://test.com"]);
    }

    public function testCreateUrlAlreadyExists()
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/draft_redirect_create_url_already_exists.json");
        $mock = new MockHandler([
            new Response(400, [], $responseData)
        ]);
        $handlerStack = HandlerStack::create($mock);
        $this->expectException(ClientException::class);
        $client = new DraftRedirect([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack
        ]);
        $client->create("test", "/test/test-redirect/", ["redirect_to" => "https://test.com"]);
    }

    public function testCreateMissingRedirectToOrDocumentId()
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/draft_redirect_create_missing_redirect_to_or_document_id.json");
        $mock = new MockHandler([
            new Response(400, [], $responseData)
        ]);
        $handlerStack = HandlerStack::create($mock);
        $this->expectException(ClientException::class);
        $client = new DraftRedirect([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack
        ]);
        $client->create("la-voz", "/test/test-redirect/", ["" => ""]);
    }

    public function testCreateRedirectTo()
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/draft_redirect_create_redirect_to.json");
        $mock = new MockHandler([
            new Response(200, [], $responseData)
        ]);
        $handlerStack = HandlerStack::create($mock);
        $client = new DraftRedirect([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack
        ]);
        $response = $client->create("test", "/test/test-redirect/", ["redirect_to" => "https://test.com"]);
        $data = json_decode($response->getBody()->getContents());
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals("2021-01-01T00:00:00.000Z", $data->created_at);
    }

    public function testCreateDocumentNotFound()
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/draft_redirect_create_document_not_found.json");
        $mock = new MockHandler([
            new Response(404, [], $responseData)
        ]);
        $this->expectException(ClientException::class);
        $handlerStack = HandlerStack::create($mock);
        $client = new DraftRedirect([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack
        ]);
        $client->create("test", "/test/test-redirect/", ["document_id" => "ABCDEFGHIJKLMNOP1234567890"]);
    }

    public function testCreateDocument()
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/draft_redirect_create_document.json");
        $mock = new MockHandler([
            new Response(200, [], $responseData)
        ]);
        $handlerStack = HandlerStack::create($mock);
        $client = new DraftRedirect([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack
        ]);
        $response = $client->create("test", "/test/test-redirect/", ["document_id" => "ABCDEFGHIJKLMNOP1234567890"]);
        $data = json_decode($response->getBody()->getContents());
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals("2021-01-01T00:00:00.000Z", $data->created_at);
    }

    public function testDeleteWebsiteNotFound()
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/draft_redirect_delete_website_not_found.json");
        $mock = new MockHandler([
            new Response(404, [], $responseData)
        ]);
        $this->expectException(ClientException::class);
        $handlerStack = HandlerStack::create($mock);
        $client = new DraftRedirect([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack
        ]);
        $client->delete("test", "/test/test-redirect/");
    }

    public function testDeleteUrlNotFound()
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/draft_redirect_delete_url_not_found.json");
        $mock = new MockHandler([
            new Response(404, [], $responseData)
        ]);
        $this->expectException(ClientException::class);
        $handlerStack = HandlerStack::create($mock);
        $client = new DraftRedirect([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack
        ]);
        $client->delete("test", "/test/test-redirect/");
    }

    public function testDeleteOk()
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/draft_redirect_delete_ok.json");
        $mock = new MockHandler([
            new Response(200, [], $responseData)
        ]);
        $handlerStack = HandlerStack::create($mock);
        $client = new DraftRedirect([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack
        ]);
        $response = $client->delete("test", "/test/test-redirect/");
        $data = json_decode($response->getBody()->getContents());
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals("/test/test-redirect/", $data->website_url);
    }

}
