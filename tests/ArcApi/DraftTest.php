<?php


namespace Test\ArcApi;

use ArcApi\Draft;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;

class DraftTest extends TestCase
{

    /**
     * @dataProvider createBadRequestDataProvider
     */
    public function testCreateBadRequest($data, $responseFile)
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/$responseFile.json");
        $mock = new MockHandler([
            new Response(400, [], $responseData)
        ]);
        $handlerStack = HandlerStack::create($mock);
        $this->expectException(ClientException::class);
        $client = new Draft([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack
        ]);
        $client->create($data);
    }

    public function testCreateOk()
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/draft_create_ok.json");
        $mock = new MockHandler([
            new Response(201, [], $responseData)
        ]);
        $handlerStack = HandlerStack::create($mock);
        $client = new Draft([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack
        ]);
        $response = $client->create(['canonical_website' => 'test', 'version' => '0.10.0', 'type' => 'story']);
        $data = json_decode($response->getBody()->getContents());
        $this->assertEquals('ABCDEFGHIJKLMNOP1234567890', $data->id);
        $this->assertEquals('2021-01-01T00:00:00.000Z', $data->created_at);
        $this->assertEquals('STORY', $data->type);
    }

    public function testViewNotFound()
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/draft_view_not_found.json");
        $mock = new MockHandler([
            new Response(404, [], $responseData)
        ]);
        $handlerStack = HandlerStack::create($mock);
        $this->expectException(ClientException::class);
        $client = new Draft([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack
        ]);
        $client->view("ABCDEFGHIJKLMNOP1234567890");
    }

    public function testViewOk()
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/draft_view_ok.json");
        $mock = new MockHandler([
            new Response(200, [], $responseData)
        ]);
        $handlerStack = HandlerStack::create($mock);
        $client = new Draft([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack
        ]);
        $response = $client->view("ABCDEFGHIJKLMNOP1234567890");
        $data = json_decode($response->getBody()->getContents());
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals("ABCDEFGHIJKLMNOP1234567890", $data->id);
        $this->assertEquals("STORY", $data->type);
    }

    public function testDeleteNotFound()
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/draft_delete_not_found.json");
        $mock = new MockHandler([
            new Response(404, [], $responseData)
        ]);
        $handlerStack = HandlerStack::create($mock);
        $this->expectException(ClientException::class);
        $client = new Draft([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack
        ]);
        $client->delete("ABCDEFGHIJKLMNOP1234567890");
    }

    public function testDeleteOk()
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/draft_delete_ok.json");
        $mock = new MockHandler([
            new Response(200, [], $responseData)
        ]);
        $handlerStack = HandlerStack::create($mock);
        $client = new Draft([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack
        ]);
        $response = $client->delete("ABCDEFGHIJKLMNOP1234567890");
        $data = json_decode($response->getBody()->getContents());
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals("ABCDEFGHIJKLMNOP1234567890", $data->id);
        $this->assertEquals("STORY", $data->type);
    }

    public function createBadRequestDataProvider()
    {
        return [
            [[], 'draft_create_bad_request_invalid_ans_json'],
            [['canonical_website' => 'test'], 'draft_create_bad_request_missing_version'],
            [['version' => '0.10.0'], 'draft_create_bad_request_missing_canonical_website'],
            [['canonical_website' => 'test', 'version' => '0.10.0'], 'draft_create_bad_request_missing_type'],
        ];
    }

}