<?php


namespace Test\ArcApi;

use ArcApi\Section;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;

class SectionTest extends TestCase
{

    public function testIndexNotFound()
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/section_index_not_found.json");
        $mock = new MockHandler([
            new Response(404, [], $responseData)
        ]);
        $handlerStack = HandlerStack::create($mock);
        $this->expectException(ClientException::class);
        $client = new Section([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack
        ]);
        $client->index("test");
    }

    public function testIndexOk()
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/section_index_ok.json");
        $mock = new MockHandler([
            new Response(200, [], $responseData)
        ]);
        $handlerStack = HandlerStack::create($mock);
        $client = new Section([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack
        ]);
        $response = $client->index("test");
        $data = json_decode($response->getBody()->getContents());
        $this->assertCount(1, $data->q_results);
        $this->assertEquals("/test", $data->q_results[0]->_id);
    }

    public function testViewWebsiteOrSectionNotFound()
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/section_view_website_or_section_not_found.json");
        $mock = new MockHandler([
            new Response(404, [], $responseData)
        ]);
        $handlerStack = HandlerStack::create($mock);
        $this->expectException(ClientException::class);
        $client = new Section([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack
        ]);
        $client->view("test", "/test");
    }

    public function testViewSectionOk()
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/section_view_ok.json");
        $mock = new MockHandler([
            new Response(200, [], $responseData)
        ]);
        $handlerStack = HandlerStack::create($mock);
        $client = new Section([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack
        ]);
        $response = $client->view("test", "/test");
        $data = json_decode($response->getBody()->getContents());
        $this->assertEquals("/test", $data->_id);
    }

    public function testCreateWebsiteNotFound()
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/section_create_website_not_found.json");
        $mock = new MockHandler([
            new Response(400, [], $responseData)
        ]);
        $handlerStack = HandlerStack::create($mock);
        $this->expectException(ClientException::class);
        $client = new Section([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack
        ]);
        $client->updateOrCreate("test", "/test", ["_id" => "/test"]);
    }

    public function testCreateBadRequest()
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/section_create_bad_request.json");
        $mock = new MockHandler([
            new Response(400, [], $responseData)
        ]);
        $handlerStack = HandlerStack::create($mock);
        $this->expectException(ClientException::class);
        $client = new Section([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack
        ]);
        $client->updateOrCreate("test", "/test", ["_id" => "/bad-test"]);
    }

    public function testCreateOk()
    {
        $responseData = file_get_contents(__DIR__ . "/../stubs/section_create_ok.json");
        $mock = new MockHandler([
            new Response(200, [], $responseData)
        ]);
        $handlerStack = HandlerStack::create($mock);
        $client = new Section([
            'org_name' => 'test',
            'auth' => ['username', 'password'],
            'handler' => $handlerStack
        ]);
        $response = $client->updateOrCreate("test", "/test", ["_id" => "/test"]);
        $data = json_decode($response->getBody()->getContents());
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals("/test", $data->_id);
    }

}