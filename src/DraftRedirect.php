<?php

namespace ArcApi;

use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;

/**
 * Class DraftRedirect
 *
 * @package ArcApi
 */
class DraftRedirect extends Client
{

    const URL = '/draft/v1/story/%s/redirect/%s';

    const URL_WEBSITE = '/draft/v1/redirect/%s%s';

    /**
     * Get story circulations
     *
     * @param string $id
     * @param string $website
     *
     * @return ResponseInterface
     */
    public function index(string $id, string $website)
    {
        return $this->getHttpClient()->get(sprintf(self::URL, $id, $website));
    }

    /**
     * Get a document's redirect
     *
     * @param string $website
     * @param string $website_url
     *
     * @return ResponseInterface
     */
    public function view(string $website, string $website_url)
    {
        return $this->getHttpClient()->get(sprintf(self::URL_WEBSITE, $website, $website_url));
    }

    /**
     * Create a document's redirect
     *
     * @param string $website
     * @param string $website_url
     * @param array $data
     *
     * @return ResponseInterface
     */
    public function create(string $website, string $website_url, array $data = [])
    {
        return $this->getHttpClient()
            ->post(sprintf(self::URL_WEBSITE, $website, $website_url), [RequestOptions::JSON => $data]);
    }

    /**
     * Delete a redirect
     *
     * @param string $id
     * @param string $website
     *
     * @return ResponseInterface
     */
    public function delete(string $id, string $website)
    {
        return $this->getHttpClient()
            ->delete(sprintf(self::URL_WEBSITE, $id, $website));
    }

}
