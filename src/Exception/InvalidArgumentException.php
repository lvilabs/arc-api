<?php

namespace ArcApi\Exception;

use InvalidArgumentException as Exception;

/**
 * Class InvalidArgumentException
 *
 * @package ArcApi\Exception
 */
class InvalidArgumentException extends Exception
{
}
