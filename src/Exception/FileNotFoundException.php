<?php

namespace ArcApi\Exception;

use Exception;

/**
 * Class FileNotFoundException
 *
 * @package ArcApi\Exception
 */
class FileNotFoundException extends Exception
{

    protected $message = "File %s not found";

    public function __construct($filename = "")
    {
        $message = sprintf($this->message, $filename);
        parent::__construct($message);
    }

}
