<?php

namespace ArcApi;

use ArcApi\Exception\FileNotFoundException;
use Psr\Http\Message\ResponseInterface;

/**
 * Class Photo
 *
 * @package ArcApi
 */
class Photo extends Client
{

    const URL = '/photo/api/v2/photos';

    /**
     * Upload
     *
     * @return ResponseInterface
     */
    public function index()
    {
        return $this->getHttpClient()->get(self::URL);
    }

    /**
     * Upload an ANS file
     *
     * @param $file
     *
     * @return ResponseInterface
     *
     * @throws FileNotFoundException
     */
    public function createByANS(string $file)
    {
        if (!file_exists($file)) {
            throw new FileNotFoundException($file);
        }
        return $this->getHttpClient()->post(self::URL, ['multipart' => [
            [
                'name' => 'ans',
                'contents' => fopen($file, 'r')
            ]
        ]]);
    }

    /**
     * Upload a file
     *
     * @param $file
     *
     * @return ResponseInterface
     *
     * @throws FileNotFoundException
     */
    public function createByFile(string $file)
    {
        if (!file_exists($file)) {
            throw new FileNotFoundException($file);
        }
        return $this->getHttpClient()->post(self::URL, ['multipart' => [
            [
                'name' => 'file',
                'contents' => fopen($file, 'r')
            ]
        ]]);
    }

    /**
     * View a Photo
     *
     * @param string $id
     *
     * @return ResponseInterface
     */
    public function view(string $id)
    {
        return $this->getHttpClient()->get(self::URL . "/" . $id);
    }

    /**
     * Publish a Photo
     *
     * @param string $id
     *
     * @return ResponseInterface
     */
    public function publish(string $id)
    {
        return $this->getHttpClient()->put(self::URL . "/" . $id . "/publish");
    }

    /**
     * Delete a Photo
     *
     * @param string $id
     *
     * @return ResponseInterface
     */
    public function delete(string $id)
    {
        return $this->getHttpClient()->delete(self::URL . "/" . $id);
    }

}
