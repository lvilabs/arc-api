<?php

namespace ArcApi;

use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;

/**
 * Class Section
 *
 * @package ArcApi
 */
class Author extends Client
{

    const URL = '/author/v2/author-service';
    const URL_UPDATE = '/author/v2/author-service/%s';

    /**
     * Create a draft
     *
     * @param array $data
     *
     * @return ResponseInterface
     */
    public function create($data = [])
    {
        return $this->getHttpClient()
            ->post(self::URL, [RequestOptions::JSON => $data]);
    }

    /**
     * Create a draft
     *
     * @param string $id
     * @param array $data
     *
     * @return ResponseInterface
     */
    public function update($id, $data = [])
    {
        return $this->getHttpClient()
            ->post(sprintf(self::URL_UPDATE,$id), [RequestOptions::JSON => $data]);
    }

    /**
     * View a draft
     *
     * @param string $id
     *
     * @return ResponseInterface
     */
    public function view(string $id)
    {
        return $this->getHttpClient()->get(self::URL . "/" . $id);
    }
}
