<?php

namespace ArcApi;

use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;

/**
 * Class Story
 *
 * @package ArcApi
 */
class Story extends Client
{

    const URL = '/story/v2/story';

    /**
     * Get a list of stories
     *
     * @return ResponseInterface
     */
    public function index()
    {
        return $this->getHttpClient()->get(self::URL);
    }

    /**
     * Create a story
     *
     * @param array $data
     *
     * @return ResponseInterface
     */
    public function create($data = [])
    {
        return $this->getHttpClient()
            ->post(self::URL, [RequestOptions::JSON => $data]);
    }

}
