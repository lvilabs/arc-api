<?php

namespace ArcApi;

use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;

/**
 * Class Draft
 *
 * @package ArcApi
 */
class Draft extends Client
{

    const URL = '/draft/v1/story';

    /**
     * Create a draft
     *
     * @param array $data
     *
     * @return ResponseInterface
     */
    public function create($data = [])
    {
        return $this->getHttpClient()
            ->post(self::URL, [RequestOptions::JSON => $data]);
    }

    /**
     * View a draft
     *
     * @param string $id
     *
     * @return ResponseInterface
     */
    public function view(string $id)
    {
        return $this->getHttpClient()->get(self::URL . "/" . $id);
    }

    /**
     * Delete a draft
     *
     * @param string $id
     *
     * @return ResponseInterface
     */
    public function delete(string $id)
    {
        return $this->getHttpClient()->delete(self::URL . "/" . $id);
    }

}
