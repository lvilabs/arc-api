<?php

namespace ArcApi;

use ArcApi\Exception\InvalidArgumentException;
use GuzzleHttp\Client as BaseClient;

/**
 * Class Client
 *
 * @package ArcApi
 */
class Client
{

    const BASE_URI_SCHEME = 'https';

    const BASE_URI_DOMAIN = 'arcpublishing.com';

    const BASE_URI_SUBDOMAIN = 'api';

    /**
     * @var BaseClient $httpClient
     */
    protected $httpClient;

    /**
     * Client constructor.
     *
     * @param array $config
     */
    public function __construct($config = [])
    {
        $subdomains = [
            self::BASE_URI_SUBDOMAIN
        ];

        if (!empty($config['sandbox']) && $config['sandbox']) {
            $subdomains[] = 'sandbox';
        }

        if (empty($config['org_name'])) {
            throw new InvalidArgumentException(
                'Required `org_name` configuration.'
            );
        }
        $subdomains[] = $config['org_name'];

        $subdomain = implode('.', $subdomains);

        $baseUri = sprintf(
            '%s://%s.%s',
            self::BASE_URI_SCHEME,
            $subdomain,
            self::BASE_URI_DOMAIN
        );

        $defaultConfig = [
            'base_uri' => $baseUri,
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
            ],
        ];
        $this->httpClient =
            new BaseClient(array_merge($defaultConfig, $config));
    }

    /**
     * @return BaseClient
     */
    public function getHttpClient()
    {
        return $this->httpClient;
    }

}
