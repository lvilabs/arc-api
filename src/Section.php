<?php

namespace ArcApi;

use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;

/**
 * Class Section
 *
 * @package ArcApi
 */
class Section extends Client
{

    const URL = '/site/v3/website/%s/section';

    /**
     * Get a list of sections
     *
     * @param string $website
     *
     * @return ResponseInterface
     */
    public function index(string $website)
    {
        return $this->getHttpClient()->get(sprintf(self::URL, $website));
    }

    /**
     * Get a section
     *
     * @param string $website
     * @param string $id
     *
     * @return ResponseInterface
     */
    public function view(string $website, string $id)
    {
        return $this->getHttpClient()->get(sprintf(self::URL, $website), ['query' => ['_id' => $id]]);
    }

    /**
     * Create or Update a section
     *
     * @param string $website
     * @param string $id
     * @param array $data
     *
     * @return ResponseInterface
     */
    public function updateOrCreate(string $website, string $id, array $data = [])
    {
        return $this->getHttpClient()
            ->put(sprintf(self::URL, $website), ['query' => ['_id' => $id], RequestOptions::JSON => $data]);
    }

}
