<?php

namespace ArcApi;

use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;

/**
 * Class DraftRevision
 *
 * @package ArcApi
 */
class DraftRevision extends Client
{

    const URL = '/draft/v1/story/%s/revision';

    /**
     * Get story revisions
     *
     * @param string $id
     *
     * @return ResponseInterface
     */
    public function index(string $id)
    {
        return $this->getHttpClient()->get(sprintf(self::URL, $id));
    }

    /**
     * Update a Document's draft Revision
     *
     * @param string $id
     * @param array $data
     *
     * @return ResponseInterface
     */
    public function update(string $id, array $data = [])
    {
        return $this->getHttpClient()->put(sprintf(self::URL, $id) . "/draft", [RequestOptions::JSON => $data]);
    }

    /**
     * Publish a Document's draft
     *
     * @param string $id
     * @param array $data
     *
     * @return ResponseInterface
     */
    public function publish(string $id, array $data = [])
    {
        return $this->getHttpClient()->post(sprintf(self::URL, $id) . "/published", [RequestOptions::JSON => $data]);
    }

}
