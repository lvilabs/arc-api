<?php

namespace ArcApi;

use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;

/**
 * Class DraftCirculation
 *
 * @package ArcApi
 */
class DraftCirculation extends Client
{

    const URL = '/draft/v1/story/%s/circulation';

    const URL_WEBSITE = '/draft/v1/story/%s/circulation/%s';

    /**
     * Get story circulations
     *
     * @param string $id
     *
     * @return ResponseInterface
     */
    public function index(string $id)
    {
        return $this->getHttpClient()->get(sprintf(self::URL, $id));
    }

    /**
     * Get a document's circulation
     *
     * @param string $id
     * @param string $website
     *
     * @return ResponseInterface
     */
    public function view(string $id, string $website)
    {
        return $this->getHttpClient()->get(sprintf(self::URL_WEBSITE, $id, $website));
    }

    /**
     * Circulate a Document
     *
     * @param string $id
     * @param string $website
     * @param array $data
     *
     * @return ResponseInterface
     */
    public function circulate(string $id, string $website, array $data = [])
    {
        return $this->getHttpClient()
            ->put(sprintf(self::URL_WEBSITE, $id, $website), [RequestOptions::JSON => $data]);
    }

}
