<?php

include_once __DIR__ . '/../vendor/autoload.php';

$client = new \ArcApi\Story([
    'sandbox' => true,
]);

$client->create(json_decode('{
  "type": "story",
  "version": "0.10.4",
  "created_date": "2019-11-18T18:52:00-03:00",
  "last_updated_date": "2019-11-18T18:52:00-03:00",
  "headlines": {
    "basic": "<I>Lorem ipsum dolor sit amet, consectetur adipiscing posuere.<\/I>"
  },
  "subheadlines": {
    "basic": "<I>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean iaculis sem est, ac lobortis orci vestibulum sed. Donec vel porttitor posuere. <\/I>"
  },
  "taxonomy": {
    "primary_section": {
      "type": "section",
      "_website": "\/$Test",
      "version": "0.10.4",
      "name": "\/$Test"
    }
  },
  "promo_items": {
    "basic": {
      "type": "image",
      "version": "0.10.4",
      "url": "test.jpg"
    }
  },
  "planning": {
    "internal_note": "Migrated from Milenium"
  },
  "workflow": {
    "status_code": 1
  },
  "content_elements": [
    {
      "type": "text",
      "content": "A Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi malesuada feugiat enim, aliquam maximus justo condimentum vel. In vulputate auctor nisl quis dapibus. Aenean metus."
    },
    {
      "type": "header",
      "content": "Lorem malesuada",
      "level": 3
    },
    {
      "type": "text",
      "content": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi malesuada feugiat enim, aliquam maximus justo condimentum vel. In vulputate auctor nisl quis dapibus. Aenean metusu."
    },
    {
      "type": "text",
      "content": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi malesuada feugiat enim, aliquam maximus justo condimentum vel. In vulputate auctor nisl quis dapibus. Aenean metusu."
    },
    {
      "type": "text",
      "content": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi malesuada feugiat enim, aliquam maximus justo condimentum vel. In vulputate auctor nisl quis dapibus. Aenean metusu."
    },
    {
      "type": "text",
      "content": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi malesuada feugiat enim, aliquam maximus justo condimentum vel. In vulputate auctor nisl quis dapibus. Aenean metusu."
    }
  ]
}', true));
